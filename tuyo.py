from tuyalinksdk.client import TuyaClient

client = TuyaClient(productid='PID', uuid='UUID', authkey='AUTHKEY')

def on_connected():
    print('Connected.')

def on_dps(dps):
    print('DataPoints:', dps)
    client.push_dps(dps)

client.on_connected = on_connected
client.on_dps = on_dps
client.connect()
client.loop_start()