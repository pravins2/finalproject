# Import libraries
import RPi.GPIO as GPIO
import time





# Set GPIO numbering mode
GPIO.setmode(GPIO.BOARD)

# Set pin 11 as an output, and define as servo1 as PWM pin
GPIO.setup(12,GPIO.OUT)
servo1 = GPIO.PWM(12,50) # pin 11 for servo1, pulse 50Hz

def setServoAngle(angle):
    duty = angle / 18 + 2.5
    servo1.ChangeDutyCycle(duty)
    time.sleep(2)

# Start PWM running, with value of 0 (pulse off)
servo1.start(0)
time.sleep(2)

setServoAngle(0)

setServoAngle(180)
print("moved 180")

# Wait for 2 seconds
#time.sleep()

# Stop the servo and cleanup GPIO
servo1.stop()
GPIO.cleanup()
