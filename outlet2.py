#!/usr/bin/env python
import time
import coloredlogs
import RPi.GPIO as GPIO
from tuyalinksdk.client import TuyaClient
from tuyalinksdk.console_qrcode import qrcode_generate
from datetime import datetime

coloredlogs.install(level='DEBUG')

MANUAL_MODE = False
LAST_FED_TIME = None
LAST_MOTION_TIME = None

client = TuyaClient(productid='cm8beguf6kdee8fo',
                    uuid='uuid89d7dc74dc21f283',
                    authkey='PzAqTaLc6PFzkadAnS7SpsyOTjoi4tB0')

def on_connected():
    print('Connected.')

def on_qrcode(url):
    qrcode_generate(url)

def on_reset(data):
    print('Reset:', data)

def on_dps(dps):
	global MANUAL_MODE
	global LAST_FED_TIME
	global LAST_MOTION_TIME

	print('DataPoints:', dps)
	if "101" in dps:
		if dps["101"] == True:
			handleServo("open")
			LAST_FED_TIME = datetime.now().strftime("%m/%d/%Y %H:%M:%S")
			dps["102"] = LAST_FED_TIME
			client.push_dps(dps)
		elif dps["101"] == False:
			handleServo("close")
	if "103" in dps:
		MANUAL_MODE = dps["103"]


	if LAST_MOTION_TIME:
		dps["104"] = LAST_MOTION_TIME

	client.push_dps(dps)

#GPIO.cleanup()

# Set GPIO numbering mode
GPIO.setmode(GPIO.BOARD)

# Set pin 11 as an output, and define as servo1 as PWM pin
GPIO.setup(12,GPIO.OUT)
servo1 = GPIO.PWM(12,50) # pin 11 for servo1, pulse 50Hz

# Start PWM running, with value of 0 (pulse off)
servo1.start(0)

angle1 = 0.0
angle2 = 180.0


#qrcode_generate("https://smartapp.tuya.com/s/p?p=jbrrlxudo0rjakmj&uuid=uuidb54fc060ddaccaec&v=2.0")

def handleServo(status):
    if status == "open":
        servo1.ChangeDutyCycle(2+(angle1/18))
    elif status == "close":
        servo1.ChangeDutyCycle(2+(angle2/18))

    time.sleep(0.5)
    servo1.ChangeDutyCycle(0)

client.on_connected = on_connected
client.on_qrcode = on_qrcode
client.on_reset = on_reset
client.on_dps = on_dps

client.connect()
client.loop_start()
#client.disconnect()

try:
    while True:
        time.sleep(1)   
except KeyboardInterrupt:
    # quit
    client.loop_stop()
